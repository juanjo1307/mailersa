<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SimpleMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $subject, $mail, $body;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $email, $body)
    {
        $this->subject = $subject;
        $this->email = $email;
        $this->body = $body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
            ->view('mail.template')
            ->with([
                'subject' => $this->subject,
                'body' => $this->body,
            ]);
    }
}
