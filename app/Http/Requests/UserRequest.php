<?php

namespace App\Http\Requests;

use App\Rules\isAdult;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method()=="POST"){
            return [
                'identifier'=>'required|numeric',
                'email'=>'required|unique:users|email:rfc,dns',
                'name'=>'required|max:100',
                'cellphone'=>'max:10',
                'identification'=>'required|max:11',
                'birthday'=>['required', new isAdult()],
                'city_id'=>'required',
                'password'=>['required',Password::min(8)->mixedCase()->letters()->numbers()->symbols()],
                'password_v'=>'required|same:password'
            ];
        }else{
            return [
                'identifier'=>'required|numeric',
                'name'=>'required|max:100',
                'cellphone'=>'max:10',
                'birthday'=>['required', new isAdult()],
                'city_id'=>'required',
                'password'=>['required',Password::min(8)->mixedCase()->letters()->numbers()->symbols()],
                'password_v'=>'required|same:password'
            ];
        }

    }
}
