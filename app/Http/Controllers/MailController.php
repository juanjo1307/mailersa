<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use App\Mail\SimpleMail;
use App\Models\Mail as Email;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $is_admin = Auth::user()->is_admin;

        if($is_admin){
            $mails = Email::with('user')->get();
        }else{
            $mails = Email::with('user')->where('user_id',Auth::id())->get();
        }
        return view('mail.index',[
            'mails'=>$mails
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mail = new Email();
        $mail->subject = $request->subject;
        $mail->email = $request->email;
        $mail->body= $request->body;
        $mail->user_id=Auth::user()->id;
        $mail->save();

        $message = (new SimpleMail($request->subject, $request->mail, $request->body))
            ->onQueue('emails');
        Mail::to($request->email)->queue($message);

//        SendEmail::dispatch($request->subject, $request->mail, $request->body);
        return redirect()->route('mails.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mail  $mail
     * @return \Illuminate\Http\Response
     */
    public function show(Mail $mail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mail  $mail
     * @return \Illuminate\Http\Response
     */
    public function edit(Mail $mail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mail  $mail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mail $mail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mail  $mail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mail $mail)
    {
        //
    }
}
