<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\City;
use App\Models\Country;
use App\Models\State;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(10);
        return view('user.index',[
            'users'=>$users
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = $this->getCountries();
        return view('user.create',[
            'countries'=>$countries
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        User::create($request->all());
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('city','state','state.country')->findOrFail($id);
        $countries = Country::get();
        $states = State::where('country_id',$user->city->state->country->id)->get();
        $cities = City::where('state_id',$user->city->state->id)->get();
        return view('user.edit',[
            'user'=> $user,
            'cities'=>$cities,
            'states'=>$states,
            'countries'=>$countries,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());
        $user->save();
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('users.index');
    }

    /**
     * Return Countries.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCountries()
    {
        return DB::table('countries')->select('id','name')->get();
    }

    /**
     * Return Countries.
     *
     * @return \Illuminate\Http\Response
     */
    public function getStates($country_id)
    {
        return DB::table('states')->select('id','country_id','name')->where('country_id',$country_id)->get();
    }
    /**
     * Return Countries.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCities($state_id)
    {
        return DB::table('cities')->select('id','state_id','name')->where('state_id',$state_id)->get();
    }

    public function search(Request $request){
        $q =$request->get('search');
        $users = User::where('name',$q)
            ->orWhere('identification',$q)
            ->orWhere('email',$q)
            ->orWhere('cellphone',$q)
            ->paginate(10);
        return view('user.index',[
            'users'=>$users
        ]);
    }

}
