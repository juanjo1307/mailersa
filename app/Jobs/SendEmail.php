<?php

namespace App\Jobs;
;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $subject, $email, $body;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($subject, $email, $body)
    {
        $this->subject = $subject;
        $this->email = $email;
        $this->body = $body;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new SendEmail($this->subject,$this->email, $this->body);
        Mail::to($this->email)->send($email);

    }
}
