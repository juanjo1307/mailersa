<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            array(
                "state_id" => 1,
                "name" => "Guayaquil",
            ),
            array(
                "state_id" => 2,
                "name" => "Quito",
            ),
            array(
                "state_id" => 3,
                "name" => "Cuenca",
            ),
        ]);
    }
}
