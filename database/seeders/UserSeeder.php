<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = 'password';
        DB::table('users')->insert([
            array(
                "identifier"=>2021,
                "email" => 'juan_jose_flores@hotmail.com',
                "password" => bcrypt($password),
                "name" => "Admin",
                "identification"=>"12345678901",
                "birthday"=>"2000-01-01",
                "city_id"=>1,
                'created_at' => now(),
                'email_verified_at' => now(),
                "is_admin"=>true,
                'remember_token' => Str::random(10),
            )]);

    }
}
