<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
            array(
                "country_id" => 1,
                "name" => "Guayas",
            ),
            array(
                "country_id" => 1,
                "name" => "Pichincha",
            ),
            array(
                "country_id" => 1,
                "name" => "Azuay",
            ),
        ]);
    }
}
