<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $password = bcrypt("password");
        return [
                "identifier"=>$this->faker->randomDigit(),
                "email" => $this->faker->unique()->safeEmail,
                "password" => $password,
                "name" =>  $this->faker->firstName,
                "identification"=>$this->faker->randomDigit(),
                "birthday"=>$this->faker->date,
                "city_id"=>$this->faker->numberBetween($min = 1, $max = 3),
                'created_at' => now(),
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
