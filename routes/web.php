<?php

use App\Http\Controllers\MailController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);


Route::middleware('auth')->group(function () {

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::middleware('isAdmin')->group(function () {
        Route::resource('users', UserController::class);
        Route::get("/search-users", [UserController::class, "search"])->name("users.search");
        Route::get("/get-countries", [UserController::class, "getCountries"])->name("users.getCountries");
        Route::get("/get-states/{country_id}", [UserController::class, "getStates"])->name("users.getStates");
        Route::get("/get-cities/{state_id}", [UserController::class, "getCities"])->name("users.getCities");
    });
    Route::resource('mails', MailController::class);

});
