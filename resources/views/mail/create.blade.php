@extends('layouts.app')
@section('title', 'Crear mails')
@section('content')
    <form method="POST" action="/mails">
        @csrf
        <div>
            <label for="subject">
                Asunto
            </label>
            <input id="subject" name="subject" type="text" placeholder="Asunto">
        </div>
        <div>
            <label for="email">
                Email
            </label>
            <input id="email" name="email" type="email" placeholder="Email">
        </div>
    <div>
        <label for="body">
            Mensaje
        </label>
        <textarea id="body" name="body" placeholder="Mensaje"></textarea>
    </div>
        <button class="btn btn-primary"
                type="submit">
            Enviar
        </button>
@endsection
