@extends('layouts.app')
@section('title', 'Mails')
@section('content')
    <a href="/mails/create" class="btn btn-primary">Crear</a>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Email</th>
            <th scope="col">Asunto</th>
            <th scope="col">Usuario</th>
            <th scope="col">Mensaje</th>
            <th scope="col">Estado</th>
        </tr>
        </thead>
        <tbody>
        @foreach($mails as $mail)
            <tr>
                <td>{{$mail->email}}</td>
                <td>{{$mail->subject}}</td>
                <td>{{$mail->user->name}}</td>
                <td>{{$mail->body}}</td>
                <td>{{$mail->status}}</td>
            </tr>
        @endforeach
        </tbody>
@endsection
