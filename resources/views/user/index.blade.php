@extends('layouts.app')
@section('title', 'Usuarios')
@section('content')
    <?php
    use Carbon\Carbon;
    ?>
    <form method="GET" action="/search-users">
        <div>
            <label for="identifier">
                Búsqueda
            </label>
            <input id="search" name="search" type="text" placeholder="Búsqueda">
            <button class="btn btn-primary"
                    type="submit">
                Buscar
            </button>
        </div>
    </form>
    <hr>
    <a href="/users/create" class="btn btn-primary">Crear</a>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Identificador</th>
            <th scope="col">Email</th>
            <th scope="col">Nombre</th>
            <th scope="col">Celular</th>
            <th scope="col">Identificación</th>
            <th scope="col">Fecha de nacimiento</th>
            <th scope="col">Edad</th>
            <th scope="col">Ciudad</th>
            <th scope="col">Acciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <th scope="row">{{$user->identifier}}</th>
                <td>{{$user->email}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->cellphone}}</td>
                <td>{{$user->identification}}</td>
                <td>{{$user->birthday}}</td>
                <td>{{Carbon::parse($user->birthday)->diffInYears(Carbon::now())}}</td>
                <td>{{$user->city->name}}</td>
                <td>
                    <a href="/users/{{$user->id}}/edit" class="btn btn-primary btn-sm">Editar</a>
                    <form action="{{ url('/users', ['id' => $user->id]) }}" method="post">
                        <input class="btn btn-danger btn-sm" type="submit" value="Borrar" />
                        @method('delete')
                        @csrf
                    </form>
            </tr>
        @endforeach
        </tbody>
    </table>
        {!! $users->links() !!}
@endsection
