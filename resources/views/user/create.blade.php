@extends('layouts.app')
@section('title', 'Crear usuarios')
@section('content')
    <form method="POST" action="/users">
        @csrf
        <div>
            <label for="identifier">
                Identificador
            </label>
            <input id="identifier" name="identifier" type="number" placeholder="Identificador">
            @error('identifier')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <label for="email">
                Email
            </label>
            <input id="email" name="email" type="text" placeholder="Email">
            @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label for="password">
                Contraseña
            </label>
            <input id="password" name="password" type="text" placeholder="Contraseña">
            @error('password')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <label for="password_v">
                Verificación
            </label>
            <input id="password_v" name="password_v" type="text" placeholder="Verificación">
            @error('password_v')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label for="name">
                Nombre
            </label>
            <input id="name" name="name" type="text" placeholder="Nombre">
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <label for="cellphone">
                Celular
            </label>
            <input id="cellphone" name="cellphone" type="text" placeholder="Celular">
            @error('cellphone')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label for="identification">
                Cédula
            </label>
            <input id="identification" name="identification" type="text" placeholder="Cédula">
            @error('identification')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <label for="birthday">
                Fecha
            </label>
            <input id="birthday" name="birthday" type="date" placeholder="Fecha">
            @error('birthday')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label for="country_id">
                País
            </label>
            <select id="country_id" name="country_id">
                <option value="" selected disabled hidden>Seleccione el país</option>
                @foreach($countries as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
            <label for="state_id">
                Estado
            </label>
            <select id="state_id" name="state_id">
                <option value="" selected disabled hidden>Seleccione el estado</option>
            </select>
            <label for="city_id">
                Estado
            </label>
            <select id="city_id" name="city_id">
                <option value="" selected disabled hidden>Seleccione la ciudad</option>
            </select>
            @error('city_id')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button class="btn btn-primary"
                type="submit">
            Enviar
        </button>
    </form>
    <script>

        $("#country_id").on('change', function (e) {
            let country = $("#country_id :selected").val()
            axios.get('/get-states/' + country).then((response) => {
                $("#state_id").empty();
                $("#state_id").append($("<option>", {
                            value: "",
                            text: "Seleccione el estado"
                        }
                    )
                )
                response.data.forEach(
                    e => $("#state_id").append($("<option>", {
                                value: e.id,
                                text: e.name
                            }
                        )
                    )
                )

            })
        });
        $("#state_id").on('change', function (e) {
            let state = $("#state_id :selected").val()
            axios.get('/get-cities/' + state).then((response) => {
                $("#city_id").empty();
                $("#city_id").append($("<option>", {
                            value: "",
                            text: "Seleccione la ciudad"
                        }
                    )
                )
                response.data.forEach(
                    e => $("#city_id").append($("<option>", {
                                value: e.id,
                                text: e.name
                            }
                        )
                    )
                )
            })
        });
    </script>
@endsection
