@extends('layouts.app')
@section('title', 'Editar usuarios')
@section('content')
    <form method="POST" action="/users/{{$user->id}}">
        @method('PUT')
        @csrf
        <div>
            <label for="identifier">
                Identificador
            </label>
            <input id="identifier" name="identifier" type="number" placeholder="Identificador" value="{{$user->identifier}}">
            @error('identifier')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <label for="email">
                Email
            </label>
            <input id="email" name="email" type="text" placeholder="Email" value="{{$user->email}}" disabled>
            @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label for="password">
                Contraseña
            </label>
            <input id="password" name="password" type="password">
            @error('password')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <label for="password_v">
                Verificación
            </label>
            <input id="password_v" name="password_v" type="text">
            @error('password_v')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label for="name">
                Nombre
            </label>
            <input id="name" name="name" type="text" placeholder="Nombre" value="{{$user->name}}">
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <label for="cellphone">
                Celular
            </label>
            <input id="cellphone" name="cellphone" type="text" placeholder="Celular" value="{{$user->cellphone}}">
            @error('cellphone')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label for="identification">
                Cédula
            </label>
            <input id="identification" name="identification" type="text" placeholder="Cédula" value="{{$user->identification}}" disabled>
            @error('identification')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <label for="birthday">
                Fecha
            </label>
            <input id="birthday" name="birthday" type="text" placeholder="Fecha" value="{{$user->birthday}}">
            @error('birthday')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label for="country_id">
                País
            </label>
            <select id="country_id" name="country_id">
                @foreach($countries as $item)
                    <option value="{{$item->id}}" @if($item->id == $user->city->state->country->id){{'selected'}}@endif>{{$item->name}}</option>
                @endforeach
            </select>
            <label for="state_id">
                Estado
            </label>
            <select id="state_id" name="state_id">
                @foreach($states as $item)
                    <option value="{{$item->id}}" @if($item->id == $user->city->state->id){{'selected'}}@endif>{{$item->name}}</option>
                @endforeach
            </select>
            <label for="city_id">
                Ciudad
            </label>
            <select id="city_id" name="city_id">
                @foreach($cities as $item)
                    <option value="{{$item->id}}" @if($item->id == $user->city->id){{'selected'}}@endif>{{$item->name}}</option>
                @endforeach
            </select>
        </div>
        <button class="btn btn-primary"
                type="submit">
            Enviar
        </button>

    </form>
    <script>

        $("#country_id").on('change', function (e) {
            let country = $("#country_id").val()
            axios.get('/get-states/' + country).then((response) => {
                $("#state_id").empty();
                $("#state_id").append($("<option>", {
                            value: "",
                            text: "Seleccione el estado"
                        }
                    )
                )
                response.data.forEach(
                    e => $("#state_id").append($("<option>", {
                                value: e.id,
                                text: e.name
                            }
                        )
                    )
                )

            })
        });
        $("#state_id").on('change', function (e) {
            let state = $("#state_id").val()
            axios.get('/get-cities/' + state).then((response) => {
                $("#city_id").empty();
                $("#city_id").append($("<option>", {
                            value: "",
                            text: "Seleccione la ciudad"
                        }
                    )
                )
                response.data.forEach(
                    e => $("#city_id").append($("<option>", {
                                value: e.id,
                                text: e.name
                            }
                        )
                    )
                )
            })
        });
    </script>
@endsection
